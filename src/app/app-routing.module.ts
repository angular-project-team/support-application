import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddRequestComponent } from './user/add-request/add-request.component';


const routes: Routes = [
  {path : '', component : AddRequestComponent},
 
  // {path: '**', redirectTo: '/page-not-found', pathMatch: 'full'}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
