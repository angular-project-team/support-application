import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablePaginationComponent } from './table-pagination/table-pagination.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [TablePaginationComponent],
  imports: [
    CommonModule,
    MDBBootstrapModule
  ],
  exports: [TablePaginationComponent, CommonModule]
})
export class SharedModule { }
