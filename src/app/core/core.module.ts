import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [NavComponent, FooterComponent],
  imports: [
    CommonModule,
    MDBBootstrapModule
  ],
  exports: [NavComponent, FooterComponent]
})
export class CoreModule { }
