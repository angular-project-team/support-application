import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ServiceName } from './service-name';

@Injectable({
  providedIn: 'root'
})
export class AppListService {
  serviceNames : Subject<ServiceName[]> = new BehaviorSubject<ServiceName[]>(null);
  PATH = 'https://swapi.co/api/people';
  PATH2 ='http://dtptest/AppReviewAPI/api/appReview/getAppReview';

  p = 'https://swapi.co/api/people/';

  issueName : Subject<string[]> = new BehaviorSubject<string[]>(null);

  constructor(private http: HttpClient) { }

  getServiceName():Observable<any>{
    return this.http.get(this.PATH);
  }

  getServiceNameData(){
    this.getServiceName().subscribe(res => {
      this.extractData(res.results[0].films);
    })
  }

  extractData(service){
    let serviceName : ServiceName[] = [];
    let count = 1;
    for (const s of service) {
      serviceName.push(new ServiceName(s,count));
      count++;
    }
    console.log(serviceName);
    this.serviceNames.next(serviceName);
  }

  //Get Issues based on the service ID;
  getIssueName(id):Observable<any>{
    return this.http.get(this.PATH2+'?appId='+id);
  }
  

  getIssueNameData(id){
    this.getIssueName(id).subscribe(res=>{
      let i : string[] = [];
      i.push(res[0].App.AppName)
      this.issueName.next(i);
      console.log(i);
     // console.log(res);
    });    
  }

}
