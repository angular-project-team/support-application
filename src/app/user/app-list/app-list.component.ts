import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AppListService } from './app-list.service';
import { ServiceName } from './service-name';
import * as $ from 'jquery';
@Component({
  selector: 'app-app-list',
  templateUrl: './app-list.component.html',
  styleUrls: ['./app-list.component.scss']
})
export class AppListComponent implements OnInit {
  serviceName: ServiceName[];
  selectedValue: number;
  issueName: string[];
  appListForm: FormGroup;
  constructor(public fb: FormBuilder, private appService: AppListService) { }

  ngOnInit() {
    this.createAppListForm();
    this.appService.getServiceNameData();
    this.appService.serviceNames.subscribe(res => {
      this.serviceName = res;
    })
  }

  createAppListForm() {
    this.appListForm = new FormGroup({
      select1: new FormControl(['']),
      select2: new FormControl('')

    })
  }

  getSelectedOptionValue() {
    this.selectedValue = $('#select').find(":selected").val();
    console.log(this.selectedValue);
    if (this.selectedValue != null) {
      this.appService.getIssueNameData(this.selectedValue);
      this.appService.issueName.subscribe(res => {
        this.issueName = res;
        console.log(this.selectedValue);
        console.log(this.issueName);
      });

    }
  }

  

  getIssue(value) {

  }
}
