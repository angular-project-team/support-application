import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthorizeRequestComponent } from './authorize-request/authorize-request.component';

import { AddRequestComponent } from './add-request/add-request.component';
import { UserRoutingModule } from './user-routing.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { AppListComponent } from './app-list/app-list.component';
import { AppFormComponent } from './app-form/app-form.component';
import { RequestListComponent } from './request-list/request-list.component';
import { HttpClientModule } from '@angular/common/http';
import { RequestStatusComponent } from './request-status/request-status.component';

@NgModule({
  declarations: [AddRequestComponent, AppListComponent, AppFormComponent, AuthorizeRequestComponent, RequestStatusComponent, AddRequestComponent, RequestListComponent],
  imports: [
    CommonModule,
    MDBBootstrapModule,
    UserRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule
  ]
})
export class UserModule { }
