import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddRequestComponent } from './add-request/add-request.component';

import { AuthorizeRequestComponent } from './authorize-request/authorize-request.component';
import { AppListComponent } from './app-list/app-list.component';
import { AppFormComponent } from './app-form/app-form.component';
import { RequestListComponent } from './request-list/request-list.component';
import { RequestStatusComponent } from './request-status/request-status.component';

const routes: Routes = [
  {path: 'add-request', component : AddRequestComponent,
  children :[
    {path: '', redirectTo : 'request-list', pathMatch: 'full' },
    {path : 'request-list', component : RequestListComponent},
    {path : 'app-list', component : AppListComponent},
    {path : 'app-form', component : AppFormComponent},
    {path : 'request-status', component : RequestStatusComponent},
    {path : 'authorize-request', component : AuthorizeRequestComponent}
  ]
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
