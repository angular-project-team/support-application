import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-app-form',
  templateUrl: './app-form.component.html',
  styleUrls: ['./app-form.component.scss']
})
export class AppFormComponent implements OnInit {

  parameterForm: FormGroup;
 
    constructor(public fb: FormBuilder) { }

  ngOnInit() {
    this.createParameterForm();
  }

  createParameterForm() {
    this.parameterForm = new FormGroup({
      Field1: new FormControl(['']),
      Field2: new FormControl('')
    })
  }
}
