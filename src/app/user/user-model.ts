export class UserModel {
    RequestDate: string;
    RequestedBy: string;
    AuthorisedDate: string;
    AuthorisedBy: string;
    Status: string;
    Issue: string;
    Service: string;


    constructor(RequestDate: string, RequestedBy: string, AuthorisedDate: string, AuthorisedBy: string, Status: string, Issue: string, Service: string)  {
        this.RequestDate = RequestDate;
        this.RequestedBy = RequestedBy;
        this.AuthorisedDate = AuthorisedDate;
        this.AuthorisedBy = AuthorisedBy;
        this.Status = Status;
        this.Issue = Issue;
        this.Service = Service;
    }
}
