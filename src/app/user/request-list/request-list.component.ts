import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { UserModel } from '../user-model';

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.scss']
})
export class RequestListComponent implements OnInit {
requestList: UserModel[];
staffId : string = "P7572";
  constructor(private userService: UserService) {
    //this.userService.request.subscribe(
    //   (list: UserModel[]) => {
    //     this.requestList = list;
    //   }
    // )
   }

  ngOnInit() {
    this.userService.getRequestListData(this.staffId);
    this.userService.requestListObservable.subscribe(request =>{
      this.requestList = request;
      console.log(this.requestList);
    })
  }

}
