import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { UserModel } from './user-model';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  PATH = 'http://localhost:58950/api/supportApp/requests';
  requestListObservable : Subject<UserModel[]> = new BehaviorSubject<UserModel[]>(null);
  constructor(private http: HttpClient) { }

  getRequestList(staffId): Observable<any> {
    return this.http.get(this.PATH+'?staffId='+staffId);
  }

  getRequestListData(staffId) {
    this.getRequestList(staffId).subscribe(res => {
      this.extractData(res);
    })
  }

  extractData(res) {
    let requestList : UserModel[] = [];
    for (const r of res) {
      requestList.push(new UserModel(
        r.RequestDate,
        r.RequestedBy,
        r.AuthorisedDate,
        r.AuthorisedBy,
        r.Status,
        r.Issue.IssueName,
        r.Issue.Service.ServiceName        
        ));
    }
    this.requestListObservable.next(requestList);
    console.log(this.requestListObservable);
  }

}
